import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Trending } from '../../../interfaces/trending.interface';
import { MoviesService } from '../../../services/movies/movies.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [MoviesService]
})
export class InicioComponent implements OnInit {
  page: number = 1;

  movies_series: Trending[] = []

  movie: Trending[] = []

  serie: Trending[] = []


  filter: string = "Todos";

  total!: number

  toSearch: string = "";

  constructor(private _moviesService: MoviesService, private _cd :ChangeDetectorRef) { }

  ngOnInit(): void {
    this.filterActivate('Todos')
  }

  getMoviesAndSeries(page:number) {
    this._moviesService.getTrending(page).subscribe({
      next: (data: any) => {
        this.movies_series = data.results;
        this.total = this.movies_series.length;
        this._cd.detectChanges();
        
      },
      error: (err) => {
        console.log(err)
      },
      complete: () => {
        console.log('La peticion termino')
      }
    })

  }

  getMovies(page:number) {
    this._moviesService.getMovies(page).subscribe({
      next: (data: any) => {
        this.movie = data.results
        this._cd.detectChanges();
      },
      error: (err) => {
        console.log(err)
      },
      complete: () => {
        console.log("Se completo la peticion en movie")
      }
    })

  };

  getSeries(page:number) {
    this._moviesService.getSeries(page).subscribe({
      next: (data: any) => {
        this.serie = data.results
        this._cd.detectChanges();
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log("Se completo la peticion en serie")
      }
    })
  };



  filterActivate(filter: string) {
    this.filter = filter;
    this._cd.detectChanges();

    switch (this.filter) {
      case 'Todos': {
        this.page = 1;
        this.getMoviesAndSeries(this.page);
        this.total = this.movies_series.length
        this._cd.detectChanges();
        break;
      }
      case 'Peliculas': {
        this.page = 1;
        this.getMovies(this.page);
        this.total = this.movie.length
        this._cd.detectChanges();
        break;
      }
      case 'Series': {
        this.page = 1;
        this.getSeries(this.page);
        this.total = this.serie.length
        this._cd.detectChanges();
        break;
      }
    }
  }

  search(value: string) {

    if (this.filter == 'Todos') {
      if (value == "") {
        this.ngOnInit();
        this.total = this.movies_series.length
      }
      else {
        this.movies_series = this.movies_series.filter(res => {
          return res.title?.toLocaleLowerCase().match(this.toSearch.toLocaleLowerCase());
        })
        this.total = this.movies_series.length
      }
    }
    else if (this.filter == 'Peliculas') {
      if (value == "") {
        this.ngOnInit();
        this.total = this.movie.length
      }
      else {
        this.movie = this.movie.filter(res => {
          return res.title?.toLocaleLowerCase().match(this.toSearch.toLocaleLowerCase());
        })
        this.total = this.movie.length
      }
    }
    else {
      if (value == "") {
        this.ngOnInit();
        this.total = this.serie.length
      }
      else {
        this.serie = this.serie.filter(res => {
          return res.name?.toLocaleLowerCase().match(this.toSearch.toLocaleLowerCase());
        })
        this.total = this.serie.length
      }
    }

  };

  onPageChange(event: any) {
    this.page = event;
    switch (this.filter) {
      case 'Todos': {
        this.getMoviesAndSeries(this.page);
        break;
      }
      case 'Peliculas': {
        this.getMovies(this.page);
        break;
      }
      case 'Series': {
        this.getSeries(this.page);
        break;
      }
    }

  }
}
