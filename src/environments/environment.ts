// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAilV5xjeS8CWbVXSfsBgdqY8CEl_DoqXc",
    authDomain: "angular-folcademy.firebaseapp.com",
    projectId: "angular-folcademy",
    storageBucket: "angular-folcademy.appspot.com",
    messagingSenderId: "238774975349",
    appId: "1:238774975349:web:425a36e09395b3d05b313c",
    measurementId: "G-2EVEFF5P2W"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
